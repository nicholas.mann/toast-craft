const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BUILD_JS_DIR = path.resolve(__dirname, 'web/js');
const BUILD_CSS_DIR = path.resolve(__dirname, 'web/css');
const DEV_DIR = path.resolve(__dirname, 'dev');

module.exports = {
    entry: [
        `${DEV_DIR}/js/index.js`, `${DEV_DIR}/scss/styles.scss`
    ],
    output: {
        path: BUILD_JS_DIR,
        filename: 'main.min.js',
        publicPath: 'web/'
    },
    devtool: 'none',
    resolve: {
        modules: [
            path.resolve('./dev/js'),
            path.resolve('node_modules')
        ]
    },
    module : {
        loaders : [
            {
                test : /\.js?/,
                include : DEV_DIR + '/js/',

                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }

            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader","sass-loader"]
                })
            },
            { test: require.resolve("jquery"), loader: "expose-loader?$!expose-loader?jQuery" },
        ]
    },
    plugins: [
        new ExtractTextPlugin("../css/styles.css"),
    ]
};