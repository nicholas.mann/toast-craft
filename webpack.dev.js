const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BUILD_JS_DIR = path.resolve(__dirname, 'web/js');
const BUILD_CSS_DIR = path.resolve(__dirname, 'web/css');
const DEV_DIR = path.resolve(__dirname, 'dev');
const WatchLiveReloadPlugin = require('webpack-watch-livereload-plugin');

const extractLess = new ExtractTextPlugin({
    filename: "../css/styles.css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: [
        `${DEV_DIR}/js/index.js`, `${DEV_DIR}/scss/styles.scss`
    ],
    output: {
        path: BUILD_JS_DIR,
        filename: 'main.min.js',
        publicPath: 'web/'
    },
    devtool: 'source-map',
    resolve: {
        modules: [
            path.resolve('./dev/js'),
            path.resolve('node_modules')
        ]
    },
    module : {
        loaders : [
            {
                test : /\.js?/,
                include : DEV_DIR + '/js/',

                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }

            },
            {
                test: /\.scss$/,
                loader: extractLess.extract({
                    fallback: "style-loader",
                    use: [{
                        loader:'css-loader',
                        options:{
                            sourceMap:true
                        }
                    },
                    {
                        loader:'sass-loader',
                        options:{
                            sourceMap:true
                        }
                    }]
                })
            },
            { test: require.resolve("jquery"), loader: "expose-loader?$!expose-loader?jQuery" },
        ]
    },
    plugins: [
        extractLess,
        new WatchLiveReloadPlugin({
            files: [
                'templates/**/*.html',
                'web/**/*.css',
                'web/**/*.js',
            ]
        }),
    ]
};