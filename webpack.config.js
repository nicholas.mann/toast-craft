var BrowserSyncPlugin = require("browser-sync-webpack-plugin");
var postcss = require('postcss-loader');

var webpackConfig = {
  // mode: "development",
  //devtool: "source-map",
  entry: __dirname + "/dev/js/index.js",
  output: {
    path: __dirname + "/web/js",
    filename: "main.min.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/, // include .js files
        use: [
          {
            loader: "babel-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "styles.css",
              outputPath: "../css"
            }
          },
          {
            loader: "extract-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: () => [require('autoprefixer')({
                'browsers': ['last 2 versions']
              })],
            }
          }
        ]
      },
      {
        // images + fonts
        test: /\.(png|jpg|gif|ttf|otf)$/,
        use: [
          {
            loader: "file-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    new BrowserSyncPlugin({
      host: "localhost",
      port: "3000",
      files: [
        "./css/styles.css",
        "./web/bundle.js",
        "./templates/**/*",
        "./templates/*"
      ],
      proxy: "http://localhost:8080"
    })
  ]
};

module.exports = webpackConfig;
