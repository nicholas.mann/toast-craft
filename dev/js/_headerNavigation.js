let _headerNavigation = {
  init: function(){
    $("header li").on('click', function () {
      let target = $(this).attr('id');
      console.log('click ', target);
      $('.dropdown-item').not(`.${target}`).hide();
      $('nav a').not($(this)).removeClass("active");
      $(`.${target}`).slideToggle("slow");
      $(this).toggleClass("active", "addOrRemove");
    });

    document.addEventListener('click', function (event) {

      // If the click happened inside the the container, bail
      if (event.target.closest('nav')) return;

      // Otherwise, run our code...
      $('.dropdown-item').slideUp("slow");
      $('nav a').removeClass("active");

    }, false);
  }
}

module.exports = _headerNavigation;